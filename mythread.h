#ifndef MYTHREAD_H
#define MYTHREAD_H

#include <QThread>
#include <QTcpSocket>
#include <QDebug>
#include <QVector>

class MyThread : public QThread
{
    Q_OBJECT
public:
    explicit MyThread(int ID, QObject *parent = 0);
    void run();

signals:
    void error(QTcpSocket::SocketError socketerror);

public slots:
    void readyRead();
    void disconnected();

public slots:

private:
    void setbit(qint16 &src, int index, bool val);
    bool getbit(qint16 src, int index);
    double my_rand(int accuracy);
    QTcpSocket *socket;
    int socketDescriptor;
    qint16 ioRegister;

    QVector<qint16> uZazor;
    QVector<qint16> iAnode;

    QVector<double> st1ph1;
    QVector<double> st1ph2;
    QVector<double> st2ph1;
    QVector<double> st2ph2;
    QVector<double> coefficients;

};

#endif // MYTHREAD_H
