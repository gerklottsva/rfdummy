#include "mythread.h"
#include <qmath.h>

MyThread::MyThread(int ID, QObject *parent) :
    QThread(parent)
{
    this->socketDescriptor = ID;
    uZazor.push_back(1500);
    uZazor.push_back(1600);
    iAnode.push_back(2000);
    iAnode.push_back(1500);
    ioRegister = 0x0C00;

    st1ph1.resize(200000);
    st1ph1.fill(0.0);
    st1ph2.resize(200000);
    st1ph2.fill(0.0);
    st2ph1.resize(200000);
    st2ph1.fill(0.0);
    st2ph2.resize(200000);
    st2ph2.fill(0.0);


    for (int i = 0; i < 105; ++i)
        coefficients.push_back(0);

    coefficients[1] = 0.117;
    coefficients[2] = -2.3;
    coefficients[3] = -2.3;
    coefficients[4] = -1.425;
    coefficients[5] = -0.39;
    coefficients[6] = -7.0;
    coefficients[7] = -7.0;
    coefficients[8] = -1.0;
    coefficients[9] = 1.455125;
    coefficients[10] = 50.0;
    coefficients[11] = 0.367;
    coefficients[12] = -2.18;
    coefficients[13] = -2.18;
    coefficients[14] = -1.514;
    coefficients[15] = -0.411;
    coefficients[16] = -8.2;
    coefficients[17] = -8.2;
    coefficients[18] = -1.0;
    coefficients[19] = 1.455125;
    coefficients[20] = 50.0;
    coefficients[21] = 0.375;
    coefficients[50] = 937.546;
    coefficients[51] = 867339;
    coefficients[52] = 436224;
    coefficients[60] = 1;
    coefficients[61] = 0;
    coefficients[62] = 835.2550;
    coefficients[70] = -27.6;
    coefficients[72] = 1.00008;
    coefficients[73] = 1.00000;
    coefficients[100] = 0.8;
    coefficients[101] = 0.7938;
    coefficients[102] = 10;
    coefficients[103] = 10;
}

void MyThread::run()
{
    //thread starts here
    qDebug() << socketDescriptor << " Starting thread";
    socket = new QTcpSocket();
    if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        emit error(socket->error());
        return;
    }

    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()),Qt::DirectConnection);
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()),Qt::DirectConnection);
    qDebug() << socketDescriptor << " Client Connected";
    exec();
}

void MyThread::readyRead()
{
    QByteArray data = socket->readAll();

    QString received(data);

    //qDebug() << socketDescriptor << " \nData in: " << received;
    if (received.contains(QString("version"), Qt::CaseInsensitive)) {
        //qDebug() << "Version query.. " ;
        QString answer("2014.12.01");
        QByteArray reply;
        reply.append(answer);
        reply.append("\r\n");
        socket->write(reply);
    }
    else if (received.contains(QString("cf"), Qt::CaseInsensitive))
    {
        QByteArray reply;
        int value=0;
        if (sscanf(data.data(), "cf %d", &value) == 1) {
            reply.append("0\r\n");
        }
        else
        socket->write(reply);
    }
    else if (received.contains(QString("st"), Qt::CaseInsensitive))
    {
        QByteArray reply;
        reply.append("1.2 2.3 3.4 4.5 1 2 3 4 5 6 7 8 9 0 11 12\r\n");
        socket->write(reply);
    }
    else if (received.contains(QString("ms"), Qt::CaseInsensitive))
    {
        QByteArray reply;
        int modNum=0;
        char modState[4];
        if (sscanf(data.data(), "ms %d %s", &modNum, modState) == 2) {
            if (((!strcmp(modState, "on") || (!strcmp(modState, "off"))) && (modNum > 0 && modNum < 3))) {
                //qDebug() << "Modulator " << modNum << " state = " << modState;
                if (!strcmp(modState, "on"))
                {
                    if (modNum == 1)
                        setbit(ioRegister, 8, true);
                    else
                        setbit(ioRegister, 9, true);
                }
                else
                {
                    if (modNum == 1)
                        setbit(ioRegister, 8, false);
                    else
                        setbit(ioRegister, 9, false);
                }
                reply.append("0\r\n");
            }
            else {
                //qDebug() << "Could not parse string " << received;
                reply.append("1\r\n");
            }
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("edit"), Qt::CaseInsensitive))
    {
        //qDebug() << "Edit controller parameters.. " ;

        QByteArray reply;
        int id = 0;
        float value = 0;
        bool failed = false;
        if (sscanf(data.data(), "edit %d %f", &id, &value) == 2) {
            if (id > coefficients.size())
                failed = true;
            else
                coefficients[id] = value;


            if (!failed)
                reply.append("0\r\n");
            else
                reply.append("1\r\n");
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);
    }
    else if (received.contains(QString("read"), Qt::CaseInsensitive)) {
        //qDebug() << "Read parameter query.. " ;
        QString answer("");
        QByteArray reply;
        int id = 0;
        double value = 0;
        bool failed = false;
        if (sscanf(data.data(), "read %d", &id) == 1) {
            if (id > coefficients.size())
                failed = true;
            else
                value = coefficients.at(id);

            if (!failed) {
                reply.append("OK ");
                reply.append(QString::number(id));
                reply.append(' ');
                reply.append(QString::number(value));
                //qDebug() << reply;
            }
            else {
                 reply.append("ERROR");
            }
        }
        else {
             reply.append("ERROR");
        }
        reply.append("\r\n");
        socket->write(reply);
    }

    else if (received.contains(QString("tbl"), Qt::CaseInsensitive))
    {
        //qDebug() << "Edit controller parameters.. " ;

        QByteArray reply;
        int stationId = 0;
        int tableId = 0;
        int index = 0;
        float value = 0;
        if (sscanf(data.data(), "tbl %d %d %d %f", &stationId, &tableId, &index, &value) == 4) {
            /*if (stationId == 1) {
                if (tableId == 1)
                {
                    st1ph1[index] = value;
                }
                else if (tableId == 2)
                {
                    st1ph2[index] = value;
                }

            }
            else if (stationId == 2) {
                if (tableId == 1)
                {
                    st2ph1[index] = value;
                }
                else if (tableId == 2)
                {
                    st2ph2[index] = value;
                }
            }
            */
            reply.append("0\r\n");
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);
    }
    else if (received.contains(QString("tsize"), Qt::CaseInsensitive))
    {
        //qDebug() << "TSize query.. " ;

        QByteArray reply;
        int stationId = 0;
        int tableId = 0;
        long size = 0;

        if (sscanf(data.data(), "tsize %d %d %d", &stationId, &tableId, &size) == 3) {
            //qDebug() << "station: " << stationId << ", table: " << tableId << ", new size: " << size;
            if (stationId == 1) {
                if (tableId == 1)
                {
                    st1ph1.resize(size);
                    st1ph1.fill(0.0);
                }
                else if (tableId == 2)
                {
                    st1ph2.resize(size);
                    st1ph2.fill(0.0);
                }
            }
            else if (stationId == 2) {
                if (tableId == 1)
                {
                    st2ph1.resize(size);
                    st2ph1.fill(0.0);
                }
                else if (tableId == 2)
                {
                    st2ph2.resize(size);
                    st2ph2.fill(0.0);
                }
            }
            reply.append("0\r\n");
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);

    }

    else if (received.contains(QString("rio"), Qt::CaseInsensitive))
    {
        //qDebug() << "RIO query.. " ;
        //qDebug() << QString::number(ioRegister, 16);

        QByteArray reply;
        reply.append("0x0");
        reply.append(QString::number(ioRegister, 16));
        reply.append("\r\n");
        socket->write(reply);
        //qDebug() << reply;
    }

    else if (received.contains(QString("wio"), Qt::CaseInsensitive))
    {
        //qDebug() << "WIO query.. " ;
        QByteArray reply;


        int outRegister = 0;
        if (sscanf(data.data(), "wio %x", &outRegister) == 1) {
            //qDebug() << QString::number(outRegister, 16) << ", decs: " << outRegister;
            ioRegister &= (qint8)outRegister;
            reply.append("0\r\n");
        }
        else {
            reply.append("1\r\n");
        }

        socket->write(reply);

    }

    else if (received.contains(QString("fast"), Qt::CaseInsensitive)) {
        //qDebug() << "Acceleration start" ;
        QString answer("0");
        QByteArray reply;
        reply.append(answer);
        reply.append("\r\n");
        socket->write(reply);
    }

    else if (received.contains(QString("save"), Qt::CaseInsensitive)) {
        //qDebug() << "Save query" ;
        QString answer("0");
        QByteArray reply;
        reply.append(answer);
        reply.append("\r\n");
        socket->write(reply);
    }

    else if (received.contains(QString("load"), Qt::CaseInsensitive)) {
        //qDebug() << "Load query" ;
        QString answer("0");
        QByteArray reply;
        reply.append(answer);
        reply.append("\r\n");
        socket->write(reply);
    }

    else if (received.contains(QString("rrs"), Qt::CaseInsensitive))
    {
        //qDebug() << "RRS query.. " ;

        QByteArray reply;
        int buffer_id = 0;
        if (sscanf(data.data(), "rrs %d", &buffer_id) == 1)
        {
            int size = 50000;
            reply.append(QString::number(size));
            reply.append("\r\n");
        }
        else
        {
            //qDebug() << "Could not parse string " << received;
            reply.append("0\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("rs"), Qt::CaseInsensitive))
    {
        //qDebug() << "RS query.. " ;

        QByteArray reply;
        int buffer_id = 0;
        if (sscanf(data.data(), "rs %d", &buffer_id) == 1) {
            int size = 50000;
            reply.append(QString::number(size));
            reply.append("\r\n");
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("0\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("rd"), Qt::CaseInsensitive))
    {
        //qDebug() << "RD query.. " ;

        QByteArray reply;
        int buffer_id = 0;
        int value_id = 0;

        if (sscanf(data.data(), "rd %d %d", &buffer_id, &value_id) == 2) {

            switch (buffer_id)
            {
            /*case 0:
            case 4:
            case 23:
            case 24:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
            case 1:
            case 2:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
            case 5:
            case 6:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
                */
            case 8:
            case 9:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
                reply.append(QString::number(value_id) + " " + QString::number(buffer_id + my_rand(6)));
                break;
            default:
                reply.append(QString::number(buffer_id + my_rand(6)));

            }
            //qDebug() << reply << endl;

        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("0");
        }
          reply.append("\r\n");
        socket->write(reply);
    }

    else if (received.contains(QString("reboot"), Qt::CaseInsensitive)) {
        //qDebug() << "Reboot query" ;
    }

    else if (received.contains(QString("su"), Qt::CaseInsensitive))
    {
        //qDebug() << "Set U zazor.. " ;

        QByteArray reply;
        int stationId = 0;
        int newV = 0;

        if (sscanf(data.data(), "su %d %d", &stationId, &newV) == 2) {
            //qDebug() << "station: " << stationId << ", new Uzazor (V): " << newV ;
            if (stationId == 1) {
                uZazor[0] = newV;
                reply.append("0\r\n");
            }
            else if (stationId == 2) {
                uZazor[1] = newV;
                reply.append("0\r\n");
            }
            else {
                //qDebug() << "There is no station with id: " << stationId;
                reply.append("1\r\n");
            }
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("si"), Qt::CaseInsensitive))
    {
        //qDebug() << "Set I anode.. " ;

        QByteArray reply;
        int stationId = 0;
        int newI = 0;

        if (sscanf(data.data(), "si %d %d", &stationId, &newI) == 2) {
            //qDebug() << "station: " << stationId << ", new Ianode (mA): " << newI ;
            if (stationId == 1) {
                iAnode[0] = newI;
                reply.append("0\r\n");
            }
            else if (stationId == 2) {
                iAnode[1] = newI;
                reply.append("0\r\n");
            }
            else {
                //qDebug() << "There is no station with id: " << stationId;
                reply.append("1\r\n");
            }
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("1\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("rab"), Qt::CaseInsensitive))
    {
        //qDebug() << "RAB query.. " ;

        QByteArray reply;
        int buffer_id = 0;
        if (sscanf(data.data(), "rab %d", &buffer_id) == 1) {

            switch (buffer_id)
            {
            case 0:
            case 4:
            case 23:
            case 24:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
            case 1:
            case 2:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
            case 5:
            case 6:
                reply.append(QString::number(buffer_id + my_rand(6)));
                break;
            default:
                reply.append(QString::number(buffer_id + my_rand(6)));
            }
            reply.append("\r\n");
        }
        else {
            //qDebug() << "Could not parse string " << received;
            reply.append("0\r\n");
        }
        socket->write(reply);
    }

    else if (received.contains(QString("ras"), Qt::CaseInsensitive))
    {
        //qDebug() << "RAS query.. " ;

        QByteArray reply;
        int stationId = 0;
        int bufferId = 0;
        if (sscanf(data.data(), "ras %d %d", &stationId, &bufferId) == 2)
        {
            int id = 0;
            if (stationId > 0 && stationId < 3)
                if (bufferId >=0 && bufferId < 8)
                {
                    reply.append(QString::number(stationId * bufferId + my_rand(6)));
                    reply.append("\r\n");
                }
                else
                    reply.append("0\r\n");
            else
                reply.append("0\r\n");

        }
        else
            reply.append("0\r\n");

        socket->write(reply);
    }

    else if (received.contains(QString("rtype"), Qt::CaseInsensitive)) {
        //qDebug() << "Rtype query" ;
        QString answer;
        QByteArray reply;
        reply.append(answer);
        reply.append("3\r\n");
        socket->write(reply);
    }
}

void MyThread::disconnected()
{
    //qDebug() << socketDescriptor << " Disconnected";

    socket->deleteLater();
    exit(0);
}

void MyThread::setbit(qint16 &src,int index, bool val){
    if (val)
        src|=(1<<index);
    else
        src&=~(1<<index);
}
bool MyThread::getbit(qint16 src, int index){
    return src&=(1<<index);
}

double MyThread::my_rand(int accuracy)
{
    double a = 0;
    a = (qrand() % int (qPow(10, accuracy) + 1))/qPow(10, accuracy);
    return a;
}

