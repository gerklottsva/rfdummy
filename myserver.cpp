#include "myserver.h"
#include "mythread.h"
#include <QDebug>

MyServer::MyServer(QObject *parent) : QTcpServer(parent)
{

}

MyServer::~MyServer()
{

}

void MyServer::StartServer()
{
    if (listen(QHostAddress::Any, 1212)) {
        qDebug() << "Started!";
    }
    else {
        qDebug() << "Not started..";
    }

}

void MyServer::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << socketDescriptor << " Connecting...";
    MyThread *thread = new MyThread(socketDescriptor,this);
    connect(thread, SIGNAL(finished()),thread, SLOT(deleteLater()));
    thread->start();
}

